import Vue from 'vue'
import VueRouter from 'vue-router'
import Signin from '@/components/Signin.vue'
import Signup from '@/components/Signup.vue'

Vue.use(VueRouter)


const routes = [
  {
    path: '/',
    name: 'Signin',
    component: Signin
  },
  {
    path: '/signup',
    name: 'Signup',
    component: Signup
  },
]

const router = new VueRouter({
  mode: 'history',
  routes
})

export default router
